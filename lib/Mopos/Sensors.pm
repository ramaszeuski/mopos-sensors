package Mopos::Sensors;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;

# Set flags and add plugins for the application
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

#    StackTrace

use Catalyst qw/
    ConfigLoader
    Static::Simple

    Authentication
    Authorization::Roles

    Session
    Session::Store::File
    Session::State::Cookie
/;

extends 'Catalyst';

our $VERSION = '1.6.0';
$VERSION = eval $VERSION;

# Configure the application.
#
# Note that settings in mopos_sensors.conf (or other external
# configuration file that you set up manually) take precedence
# over this when using ConfigLoader. Thus configuration
# details given here can function as a default configuration,
# with an external configuration file acting as an override for
# local deployment.

__PACKAGE__->config(
    name => 'Mopos::Sensors',
    encoding => 'utf8',

    # Disable deprecated behavior needed by old applications
    using_frontend_proxy => 1,
    disable_component_resolution_regex_fallback => 1,

    authentication => {
      default_realm => 'users',
      realms        => {
         users => {
            credential => {
               class          => 'Password',
               password_field => 'password',
               password_type  => 'clear'
            },
            store => {
               class         => 'DBIx::Class',
               user_model    => 'DB::Account',
               role_column   => 'roles',
               id_field      => 'login',
            }
         },
         nopassword => {
            credential => {
               class          => 'NoPassword',
            },
            store => {
               class         => 'DBIx::Class',
               user_model    => 'DB::Account',
               role_column => 'roles',
               role_field    => 'role',
            }
         },
      },
   },

   'Plugin::Static::Simple' => {
        include_path => [
            'root/static',
        ],
    },

    'Plugin::Session' => {
        cookie_expires => 0,
        cookie_secure  => 0,
    },

    'Controller::HTML::FormFu' => {
        constructor => {
            render_method => 'tt',
            tt_args       => {
                ENCODING => 'utf-8',
            },
        },
        model_stash => {
            schema => 'DB',
        },
    },

    'Model::DB' => {
        connect_info => {
            dsn      => $ENV{DB_DSN} // 'dbi:Pg:dbname=sensors;host=sql',
            user     => $ENV{DB_USERNAME} // 'sensors',
            password => $ENV{DB_PASSWORD} // 'password',
        }
    },

    inbox    => $ENV{INBOX},

);

# Start the application
__PACKAGE__->setup();


=head1 NAME

Mopos::Sensors - Catalyst based application

=head1 SYNOPSIS

    script/mopos_sensors_server.pl

=head1 DESCRIPTION

[enter your description here]

=head1 SEE ALSO

L<Mopos::Sensors::Controller::Root>, L<Catalyst>

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
