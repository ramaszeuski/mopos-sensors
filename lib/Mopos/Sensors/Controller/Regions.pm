package Mopos::Sensors::Controller::Regions;
use Moose;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller'; }

use Spreadsheet::WriteExcel;
use Date::Manip;
use File::Basename;
use File::Temp qw( tempfile );

my $dir = dirname(__FILE__);
Date_Init('ConfigFile=' . "$dir/../../../../etc/DateManip");

=head1 NAME

Mopos::Sensors::Controller::Regions - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

=head2 base

=cut

sub base
:Chained('/')
:PathPart('regions')
:CaptureArgs(0)
{
    my ($self, $c) = @_;
}

=head2 region

=cut

sub region
:Chained('base')
:PathPart('')
:CaptureArgs(1)
{
    my ($self, $c, $id) = @_;
    my $region = $c->model('DB::Region')->find($id);

    $c->stash(
        region => $region,
        title  => $region->name,
    );
}

sub view
:Chained('region')
:PathPart('')
:Args(0)
{
    my ( $self, $c ) = @_;

    my $sensors = $c->stash->{region}->sensors(
        {
            active => 1
        },
        {
            order_by => [
                { '-asc'  => 'center_name', },
                { '-desc' => 'priority', },
                { '-asc'  => 'description', },
                { '-asc'  => 'name', },
            ]
        }
    );

    REGION:
    while ( my $sensor = $sensors->next() ) {
        push @{ $c->stash->{sensors} }, $sensor;
    }

}


sub export
:Chained('region')
:PathPart('export')
:Args(0)
{
    my ( $self, $c ) = @_;

    my $args = $c->request->params();

    my (undef, $file) = tempfile();
    my $workbook      = Spreadsheet::WriteExcel->new($file);

    my $begin = UnixDate( ParseDate($args->{begin_date}), '%Y-%m-%d 00:00:00');
    my $end   = UnixDate( ParseDate($args->{end_date}),   '%Y-%m-%d 23:59:59');

    my $sensors = $c->stash->{region}->sensors(
        {
            active => 1,
            id     => $args->{sensor_id},
        },
        {
            order_by => [
                { '-asc'  => 'center_name', },
                { '-desc' => 'priority', },
                { '-asc'  => 'name', },
            ]
        }
    );

    SENSOR:
    while ( my $sensor = $sensors->next() ) {

        my $worksheet_name = $sensor->fullname_ascii;
        $worksheet_name    =~ s/^.+:\s+//;

        my $worksheet = $workbook->add_worksheet(
            substr($worksheet_name, 0, 31)
        );

	    my $row = 0;

        my $values = $sensor->values(
            {
               timestamp => {
                   '>=' => $begin,
                   '<=' => $end,
                },
            },
            {
                order_by => [ "timestamp" ],
            },
        );

        VALUES:
        while ( my $value = $values->next() ) {
            $worksheet->write_date_time(
                $row, 0, UnixDate($value->timestamp, '%d.%m.%Y %H:%M')
            );
            $worksheet->write_number(
                $row++, 1, $value->value
            );

        } #VALUES
    } #SENSORS

    $workbook->close();

    $c->response->header(
        'Content-Disposition',
        'attachment; filename="cidla.xls"',
    );
    $c->response->content_type( 'application/vnd.ms-excel' );
    $c->serve_static_file( $file );

    unlink $file;
}

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
