package Mopos::Sensors::Controller::Logout;
use Moose;
use namespace::autoclean;
use utf8;

BEGIN {extends 'Catalyst::Controller'; }

=head1 NAME

Mopos::Sensors::Controller::Logout - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :
Path
Args(0)
Menu('Odhlásit se')
MenuOrder(1000)
{
    my ( $self, $c ) = @_;
    $c->delete_session('session expired');
    $c->logout;
    $c->response->redirect('/');
}


=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
