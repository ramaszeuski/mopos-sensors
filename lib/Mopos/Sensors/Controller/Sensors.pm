package Mopos::Sensors::Controller::Sensors;
use Moose;
use namespace::autoclean;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use Date::Manip;
use Math::Round qw(nearest);
use YAML;
use utf8;

=head1 NAME

Mopos::Sensors::Controller::Sensors - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub base
:Chained('/')
:PathPart('sensors')
:CaptureArgs(0)
{
    my ($self, $c) = @_;
    my $args = $c->request->params();
}

=head2 sensor

=cut

sub sensor
:Chained('base')
:PathPart('')
:CaptureArgs(1)
{
    my ($self, $c, $id) = @_;

    my $sensor = $c->model('DB::Sensor')->find($id);

    $c->stash(
        sensor => $sensor,
    );
}

=head2 graph

=cut

sub graph
:Chained('sensor')
:PathPart('graph')
:Args(0)
{
    my ( $self, $c ) = @_;

    $c->stash->{no_wrapper} = 1;

    my $args   = $c->request->params();
    my $sensor = $c->stash->{sensor};

    my $filter = _set_filter( $c );

    my $date_begin = DateCalc('now', sprintf('%d days', $filter->{offset} - $filter->{days}));
    my $date_end   = DateCalc('now', sprintf('%d days', $filter->{offset}));

    my ( $conditions, $attributes, $values );

    $conditions = {
        timestamp => {
            '>=' => UnixDate($date_begin, '%Y-%m-%d %H:%M'),
            '<=' => UnixDate($date_end,   '%Y-%m-%d %H:%M'),
        },
    };

    if ( $sensor->type =~ /^[134567]$/ ) {
        if ( 1 || $c->session->{detail}{ $sensor->id } ) {
            $attributes = {
                select   => [ \q{date_trunc('min',"timestamp")}, { max => 'value' } ],
                as       => [ 'hour', 'value' ],
                group_by => [ \q{date_trunc('min',"timestamp")} ],
                order_by => [ \q{date_trunc('min',"timestamp")} ],
            };
        }
        else {
            $attributes = {
                select   => [ \q{date_trunc('hour',"timestamp")}, { avg => 'value' } ], #u hladin - soucet???
                as       => [ 'hour', 'value' ],
                group_by => [ \q{date_trunc('hour',"timestamp")} ],
                order_by => [ \q{date_trunc('hour',"timestamp")} ],
            };
        }
    }
    elsif ( $sensor->type =~ /^[2]$/ ) {
       $attributes = {
           select   => [ \q{date_trunc('hour',"timestamp")}, { sum => 'value' } ],
           as       => [ 'hour', 'value' ],
           group_by => [ \q{date_trunc('hour',"timestamp")} ],
           order_by => [ \q{date_trunc('hour',"timestamp")} ],
       };
    }

    $values = $sensor->values(
        $conditions,
        $attributes,
    );

    my @values = ();
    my ( $min, $max ) = ( 0, 0 );


    VALUE:
    while ( my $value = $values->next() ) {
        my %value = $value->get_columns;
        push @values, \%value;
        $min = $value{value} if $value{value} < $min;
        $max = $value{value} if $value{value} > $max;
    }

    # Korekce nepovolenych hodnot
    VALUE:
    foreach my $p (0 .. $#values) {
        if (
            ($sensor->max_value && $values[$p]->{value} > $sensor->max_value)
            ||
            ($sensor->min_value && $values[$p]->{value} < $sensor->min_value)
        ) {
            $values[$p]->{raw_value} = $values[$p]->{value};

            if ( $p == 0 ) {
                $values[$p]->{value} = $values[1]->{value};
            } elsif ( $p == $#values ) {
                $values[$p]->{value} = $values[$p - 1]->{value};
            } else {
                $values[$p]->{value} = ($values[$p - 1]->{value} + $values[$p + 1]->{value}) / 2;
            }
        }
    }

    $c->stash(
        min        => nearest(.1, $min),
        max        => nearest(.1, $max),
        values     => \@values,
        count      => scalar @values,
        days       => $filter->{days},
        date_begin => UnixDate($date_begin, '%Y-%m-%d %H:%M'),
        date_end   => UnixDate($date_end,   '%Y-%m-%d %H:%M'),
        date_begin_formatted => UnixDate($date_begin, '%d.%m.%Y %H:%M'),
        date_end_formatted   => UnixDate($date_end,   '%d.%m.%Y %H:%M'),
    );


}

=head2 set_filter

=cut

sub set_filter
:Chained('sensor')
:PathPart('set_filter')
:Args(0)
{
    my ( $self, $c ) = @_;

    _set_filter( $c, $c->request->params() );

    $c->response->body('OK');
}

sub _set_filter {
    my ( $c, $args ) = @_;


    my $id = $args->{id} // $c->stash->{sensor}->id;

    if ( exists $args->{detail} ) {
        $c->session->{detail}{$id} = $args->{detail};
        return;
    }

    if ( ! exists $c->session->{filter}{$id}) {
        $c->session->{filter}{$id} = {
            offset => 0,
            days   => 7,
        };
    }

    if ( exists $args->{offset} ) {
        if ( $args->{offset} ) {
            if ( $args->{back}) {
                $c->session->{filter}{$id}{offset} -= $args->{offset};
            }
            else {
                $c->session->{filter}{$id}{offset} += $args->{offset};
            }
        }
        else {
            $c->session->{filter}{$id}{offset} = $args->{offset};
        }
    }

    if ( exists $args->{days} ) {
        $c->session->{filter}{$id}{days} = $args->{days};
    }

    return $c->session->{filter}{$id};

}

sub edit :Chained('sensor') :PathPart('edit') :Args(0) :FormConfig('sensor.yaml') {
    my ( $self, $c ) = @_;

    if ( ! $c->check_any_user_role( qw(sensors) ) ) {
        $c->stash->{template} = 'denied.tt2';
        return;
    }

}

sub edit_FORM_RENDER {
    my ( $self, $c ) = @_;

    my $form   = $c->stash->{form};
    my $sensor = $c->stash->{sensor};

    $form->get_field( {name => 'type'} )
        ->options(
            $c->model('DB::Sensor')->types()
        )
#        ->value($se)
    ;

    $form->model->default_values($sensor);
}

sub edit_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form    = $c->stash->{form};
    my $sensor  = $c->stash->{sensor};
    my $args    = $c->request->params;

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

#    $c->model('DB::Log')->create(
#        {
#            event   => 'abonent_edit',
#            user_id => $c->user->id,
#            address => $c->req->address,
#            data    => $abonent->id,
#        }
#    );

    $form->model->update($sensor);

    $scope_guard->commit;

    $c->response->redirect( '/regions/' . $sensor->center->region_id );
    $c->detach;
}

sub receivers :Chained('sensor') :PathPart('receivers') :Args(0) {
    my ($self, $c) = @_;

    $c->stash->{receivers} = $c->stash->{sensor}->sms_receivers();

    $c->stash->{no_wrapper} = 1;
}

sub add_receiver :Chained('sensor') :PathPart('add_receiver') :Args(0) {
    my ($self, $c) = @_;

    my $args = $c->request->params;
    my $sensor = $c->stash->{sensor};

    if ( $args->{phone} !~ /^\d{9}$/ ) {
        $c->stash(
            output => 'json',
            json    => {
                error => 'Neplatné telefonní číslo',
            },
        );
        return;
    }

    if ( $args->{spa} !~ /^[123]$/ ) {
        $c->stash(
            output => 'json',
            json    => {
                error => 'Neplatný SPA',
            },
        );
        return;
    }

    $sensor->add_to_sms_receivers(
        {
            phone => $args->{phone},
            name  => $args->{name},
            spa   => $args->{spa},
        }
    );

    $c->stash(
        output => 'json',
        json    => {
            rc => 'OK',
        },
    );

}

sub save_receiver :Chained('base') :PathPart('save_receiver') :Args(0) {
    my ( $self, $c ) = @_;

    my $args = $c->request->params;

#    if ( $args->{name} eq 'phone' && $args->{value} !~ /^\d{9}$/ ) {
#        $c->stash(
#            output => 'json',
#            json    => {
#                error => 'Neplatné telefonní číslo',
#            },
#        );
#        return;
#    }

    my $receiver = $c->model('DB::SMS_Receiver')->find(
        { id => $args->{pk}, }
    );

    my $scope_guard = $c->model('DB')->schema->txn_scope_guard;

    $receiver->update( {
        $args->{name} => $args->{value},
    } );

    $scope_guard->commit;

    $c->stash(
        output => 'json',
        json    => {
            rc => 'OK',
        },
    );
}

sub delete_receiver :Chained('sensor') :PathPart('delete_receiver') :Args(0) {
    my ($self, $c) = @_;

    my $args   = $c->request->params;
    my $sensor = $c->stash->{sensor};

    $sensor->sms_receivers(
        { id => $args->{id} },
    )->delete;

    $c->stash(
        output => 'json',
        json    => {
            rc => 'OK',
        },
    );

}

sub summary :Chained('sensor') :PathPart('summary') :Args(0) {
    my ($self, $c) = @_;

    my $args = $c->request->params;
    my $sensor = $c->stash->{sensor};

    my $date_begin = ParseDate( $args->{date_begin});
    my $date_end   = ParseDate( $args->{date_end});

    if ( ! ( $date_begin && $date_end )) {
        $c->stash(
            output => 'json',
            json    => {
                rc => 'Neplatné období',
            },
        );
        return;
    }

    my $summary = $sensor->values(
        {
            timestamp => {
                between => [
                    UnixDate($date_begin, '%Y-%m-%d 00:00:00'),
                    UnixDate($date_end,   '%Y-%m-%d 23:59:59'),
                ]
            }
        },
        {
           select   => [ { sum => 'value' } ],
           as       => [ 'summary' ],
        }
    )->first;

    $summary = $summary->get_column('summary') || 0;

    $c->stash(
        output => 'json',
        json    => {
            rc => "$summary mm",
        },
    );

}

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__



