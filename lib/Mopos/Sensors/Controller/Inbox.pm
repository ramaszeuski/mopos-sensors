package Mopos::Sensors::Controller::Inbox;
use Moose;
use namespace::autoclean;
use Params::Validate;
use Digest::MD5 qw(md5_hex);
use Mopos::Sensors::SMS;
use Date::Manip;
use YAML;

BEGIN {extends 'Catalyst::Controller::REST'; }

use constant ERROR404   => '404 SENSOR NOT FOUND';
use constant ERROR401   => '401 INVALID SIGNATURE';
use constant REGEXP_VOX1 => qr/digipp_measure$/i;
use constant REGEXP_VOX2 => qr/SenzorRecords$/i;

=head1 NAME

Mopos::Sensors::Controller::Inbox - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut

sub vox : Local : ActionClass('REST') {}

sub vox_POST {
    my ( $self, $c ) = @_;

    my $args = $c->request->data;

    my $now = UnixDate('now', '%Y%m%d%H%M%S');
    my $uri = $c->req->uri;

    if ( $ENV{DEBUG_VOX_POST} ) {
        open OUT, "+>", "/tmp/sensors/$now.src";
        print OUT $c->req->uri . "\n";
        print OUT Dump $args;
        close OUT;
    }

    my @data = ();

    if ( $uri =~ REGEXP_VOX1 && $args->{data} && ref $args->{data} eq 'ARRAY' ) {
        @data = @{ $args->{data} };
    }
    elsif ( $uri =~ REGEXP_VOX2 && $args->{CityID} && $args->{Address} ) {
        $args->{location}  = $args->{CityID};
        @data = (
            {
                address   => $args->{Address},
                timestamp => $args->{TimestampString},
                val1      => $args->{Channel1},
            },
        );
    }

    if ( $ENV{DEBUG_VOX_POST} ) {
        open OUT, "+>", "/tmp/sensors/$now.dat";
        print OUT Dump \@data;
        close OUT;
    }

    SENSOR:
    foreach my $data ( @data ) {

        my $sensor = $c->model('DB::Sensor')->search(
            {
                md5id => $args->{location} . '.' . $data->{address}
                #TODO: login+password
            }
        )->first;

        next SENSOR if ! $sensor;

        my $out_path = join '/', $c->config->{inbox}, $sensor->center_id;
        make_path($out_path) if ! -d $out_path;
        my $filename = "$out_path/" . $sensor->id . '_' . UnixDate($data->{timestamp}, '%Y%m%d_%H%M'). '.txt';

        open OUT, "+>", $filename;
        if ( ! -s $filename ) {
            print OUT "#Name " . $sensor->md5id . "\n";
        }
        print OUT UnixDate($data->{timestamp}, '%d.%m.%Y %H:%M:%S') . ' ' . $data->{val1} . "\n";
        close OUT;
    }

    $self->status_ok(
        $c,
        entity => {},
    );
}

=head2 index

=cut

=head1 AUTHOR

Andrej Ramaszeuski,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;

__END__

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;

    my %args = ();

    eval {
        %args  = Params::Validate::validate_with (
            params      => $c->request->params,
            spec        => {
                id        => {},
                timestamp => { regex => qr/^\d{4}\-\d{2}\-\d{2}\s\d\d:\d\d$/ },
                value     => { regex => qr/^\-?\d+(\.\d+)*$/ },
                hash      => {},
            },
            allow_extra => 1,
        );
    };

    if ( $@ ) {
        my $error = $@;
        $error =~ s/\n.*//s;
        $c->response->body( "412 $error" );
        return;
    }

    my $sensor = $c->model('DB::Sensor')->search(
        {
            md5id => $args{id},
        }
    )->first;

    if ( ! $sensor ) {
        $c->response->body( ERROR404 );
        return;
    }

    my $hash = md5_hex($args{id} . $args{timestamp} . $args{value} . $sensor->api_key);

    if ( $hash ne $args{hash}) {
        $c->response->body( ERROR401 );
       return;
    }

    $self->store_value(
        $c,
        {
            sensor    => $sensor,
            value     => $args{value},
            timestamp => $args{timestamp},
        }
    );

    $c->response->body('OK');

}

