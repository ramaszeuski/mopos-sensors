package Mopos::Sensors::Controller::Login;
use Moose;
use namespace::autoclean;

use Net::LDAP;
use Net::LDAP::Bind;

BEGIN {extends 'Catalyst::Controller::HTML::FormFu'; }

use constant REDIRECT_DEFAULT => '/';

=head1 NAME

Mopos::Sensors::Controller::Login - Catalyst Controller

=head1 DESCRIPTION

Catalyst Controller.

=head1 METHODS

=cut


=head2 index

=cut

sub index :Path :Args(0) :FormConfig('login.yaml') {}

sub index_FORM_VALID {
    my ( $self, $c ) = @_;

    my $form = $c->stash->{form};

    my $login    = $form->param('login');
    my $password = $form->param('password');

    my @auth = ({
        login    => $login,
        password => $password,
        deleted  => undef,
    });

    if ($ENV{LDAP_SERVER}) {
        my $id = $self->_ldap_auth($c, {
            login    => $login,
            password => $password,
        });

        if ($id) {
            @auth = ({ id => $id }, 'nopassword');
        }
    }

    if ($c->authenticate(@auth)) {
        my $redirect = REDIRECT_DEFAULT;
        $c->response->redirect($c->uri_for($redirect));
        return;
    }
    else {
        $form->get_field('password')
            ->get_constraint({ type => 'Callback' })
            ->force_errors(1);
        $form->process;
    }

}

sub _ldap_auth {
    my ($self, $c, $args )  = @_;

    my $ldap = Net::LDAP->new($ENV{LDAP_SERVER}) // return; # TODO: Trace error
    my $dn   = sprintf($ENV{LDAP_USER_DN}, $args->{login});

    my $bind_result = $ldap->bind(
        $dn,
        password => $args->{password}
    );

    if ($bind_result->code) {
        return; #TODO: trace error
    }

    my $search_result = $ldap->search(
        base   => $dn,
        scope  => 'base',
        filter => '(objectClass=*)',
    );

    if ($search_result->code) {
#        $c->app->log->debug($search_result->error);
        return;
    }

    my $ldap_user = $search_result->entry(0) // return;

    my $user = $c->model('DB::Account')->update_or_create(
        {
            login    => $args->{login},
            password => '',
            roles    => 'sensors'
        },
        { key => 'login' }
    );

#        $c->journal('LDAP user', { ldap_dn => $dn });

    $ldap->unbind;

    return $user->id;
}
=head1 AUTHOR

Andrej Ramašeuski

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

__PACKAGE__->meta->make_immutable;

1;
