package Mopos::Sensors::Schema::Result::Account;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('accounts');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        login
        password
        roles
    )
 );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    'login' => [qw(login)]
);

1;
