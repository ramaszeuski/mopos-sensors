package Mopos::Sensors::Schema::Result::Center;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('centers');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        region_id
        active
        priority
        name
        description
        ftp_login
        ftp_password
        data_format

        weatherlink_login
        weatherlink_password
        weatherlink_api_key
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    'region',
    'Mopos::Sensors::Schema::Result::Region',
    'region_id',
);

__PACKAGE__->has_many(
    'sensors',
    'Mopos::Sensors::Schema::Result::Sensor',
    'center_id',
);

__PACKAGE__->has_many(
    'files',
    'Mopos::Sensors::Schema::Result::File',
    'center_id',
);

1;

__END__
