package Mopos::Sensors::Schema::Result::SMS_Alert;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('sms_alerts');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        value_id
        created
        phone
        message
        rc
        price
        sms_id
        delivery_time
        delivery_status
    ),
);
__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    'value',
    'Mopos::Sensors::Schema::Result::Value',
    'value_id',
);

1;


