package Mopos::Sensors::Schema::Result::Customer;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('customers');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        name
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    'customer_phones',
    'Mopos::Sensors::Schema::Result::Customer_Phone',
    'customer_id',
);

1;

__END__
