package Mopos::Sensors::Schema::Result::Value;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

use JSON;

our $VERSION = 1;

__PACKAGE__->table('values');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        sensor_id
        alias_id
        timestamp
        value
        voltage
        sliding_sum
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->add_unique_constraint(
    unique_value => [qw(sensor_id timestamp value)]
);

__PACKAGE__->belongs_to(
    'sensor',
    'Mopos::Sensors::Schema::Result::Sensor',
    'sensor_id',
);

__PACKAGE__->has_many(
    'alerts',
    'Mopos::Sensors::Schema::Result::SMS_Alert',
    'value_id',
);

__PACKAGE__->inflate_column('sliding_sum', {
    inflate => sub { from_json(shift); },
    deflate => sub { to_json(shift); },
});

1;

__END__
