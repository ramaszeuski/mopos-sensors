package Mopos::Sensors::Schema::Result::Sensor_Alias;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

__PACKAGE__->table('sensors_aliases');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        sensor_id
        active
        md5id
        factor
        offset
        api_key
        tz
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    'sensor',
    'Mopos::Sensors::Schema::Result::Sensor',
    'sensor_id',
);

1;

__END__


