package Mopos::Sensors::Schema::Result::Customer_Phone;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('customers_phones');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        customer_id
        phone
        name
    )
 );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    'customer',
    'Mopos::Sensors::Schema::Result::Customer',
    'customer_id',
);

1;

__END__
