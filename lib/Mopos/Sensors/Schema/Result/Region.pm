package Mopos::Sensors::Schema::Result::Region;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('regions');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        active
        priority
        name
        description
        google_maps_center
        google_maps_scale
    )
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->has_many(
    'centers',
    'Mopos::Sensors::Schema::Result::Center',
    'region_id',
);

__PACKAGE__->has_many(
    'sensors',
    'Mopos::Sensors::Schema::Result::Sensor_view',
    'region_id',
);

1;

__END__
