package Mopos::Sensors::Schema::Result::Sensor;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

use JSON;

__PACKAGE__->table('sensors');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        md5id
        center_id
        active
        fault
        obsolete
        priority
        name
        description
        gps
        unit
        alert1
        alert2
        alert3
        ceiling
        last_change
        last_value
        last_alert
        pre_alert
        factor
        offset
        api_key
        tz
        type
        rain_watchers
        column_value
        column_voltage
        max_value
        min_value
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    'center',
    'Mopos::Sensors::Schema::Result::Center',
    'center_id',
);

__PACKAGE__->has_many(
    'values',
    'Mopos::Sensors::Schema::Result::Value',
    'sensor_id',
);

__PACKAGE__->has_many(
    'sms_receivers',
    'Mopos::Sensors::Schema::Result::SMS_Receiver',
    'sensor_id',
);

__PACKAGE__->inflate_column('rain_watchers', {
    inflate => sub { from_json(shift); },
    deflate => sub { to_json(shift); },
});


sub alerts_defined {
    my $self = shift;
    return 1 if defined $self->alert1 && $self->alert1 > 0;
    return 1 if defined $self->alert2 && $self->alert1 > 0;
    return 1 if defined $self->alert3 && $self->alert1 > 0;
    return 0;
}

sub value_name {
    my $self = shift;

    # dirty hack - bez alertu zatim jen srazkomer

    if ( $self->alerts_defined ) {
        return 'Výška hladiny';
    }
    else {
        return $self->description;
    }
}

sub google_maps_center {

    my $self = shift;

    if ( $self->gps =~ /^\d+\.\d+,\s*\d+\.\d+$/ ) {
        return $self->gps;
    }
	elsif ( $self->gps =~ /(\d+)°(\d+)'([\d\.]+)"N,\s*(\d+)°(\d+)'([\d\.]+)"E/ ) {
        return ($1 + $2/60 + $3/3600 ) . ', ' . ($4 + $5/60 + $6/3600);
    }
    else {
        return undef;
    }

}

sub spa {
    my $self = shift;
    return 0 if ! $self->alerts_defined();
    return 0 if ! $self->last_value;
    return 3 if $self->last_value > $self->alert3;
    return 2 if $self->last_value > $self->alert2;
    return 1 if $self->last_value > $self->alert1;
    return 0;
}


1;

__END__

sub fullname {
    my $self = shift;
	return $self->name . ' - ' . $self->description;
}

sub fullname_ascii {
    my $self = shift;
	my $fullname = $self->fullname;

    $fullname =~ s/á/a/g;
    $fullname =~ s/č/c/g;
    $fullname =~ s/ď/d/g;
    $fullname =~ s/é/e/g;
    $fullname =~ s/ě/e/g;
    $fullname =~ s/í/i/g;
    $fullname =~ s/ĺ/l/g;
    $fullname =~ s/ň/n/g;
    $fullname =~ s/ó/o/g;
    $fullname =~ s/ô/o/g;
    $fullname =~ s/ö/o/g;
    $fullname =~ s/ř/r/g;
    $fullname =~ s/š/s/g;
    $fullname =~ s/ť/t/g;
    $fullname =~ s/ú/u/g;
    $fullname =~ s/ů/u/g;
    $fullname =~ s/ý/y/g;
    $fullname =~ s/ž/z/g;
    $fullname =~ s/Á/A/g;
    $fullname =~ s/Č/C/g;
    $fullname =~ s/Ď/D/g;
    $fullname =~ s/É/E/g;
    $fullname =~ s/Ě/E/g;
    $fullname =~ s/Í/I/g;
    $fullname =~ s/Ĺ/L/g;
    $fullname =~ s/Ľ/L/g;
    $fullname =~ s/ľ/l/g;
    $fullname =~ s/Ň/N/g;
    $fullname =~ s/Ó/O/g;
    $fullname =~ s/Ô/O/g;
    $fullname =~ s/Ö/O/g;
    $fullname =~ s/Ř/R/g;
    $fullname =~ s/Š/S/g;
    $fullname =~ s/Ť/T/g;
    $fullname =~ s/Ú/U/g;
    $fullname =~ s/Ů/U/g;
    $fullname =~ s/Ý/Y/g;
    $fullname =~ s/Ž/Z/g;

    return $fullname;
}
