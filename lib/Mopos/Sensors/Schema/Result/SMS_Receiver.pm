package Mopos::Sensors::Schema::Result::SMS_Receiver;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('sms_receivers');

__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        sensor_id
        spa
        phone
        name
        active
    ),
);

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    'sensor',
    'Mopos::Sensors::Schema::Result::Sensor',
    'sensor_id',
);

1;

__END__
