package Mopos::Sensors::Schema::Result::File;

use base 'DBIx::Class::Core';

use strict;
use warnings;
use utf8;

our $VERSION = 1;

__PACKAGE__->table('files');
__PACKAGE__->add_columns(
    id => {
        sequence     => 'uid_seq',
        auto_nextval => 1,
    },
    qw(
        center_id
        processed
        md5
        content
    )
 );

__PACKAGE__->set_primary_key('id');

__PACKAGE__->belongs_to(
    'center',
    'Mopos::Sensors::Schema::Result::Center',
    'center_id',
);

1;

__END__
