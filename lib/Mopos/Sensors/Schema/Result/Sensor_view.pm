package Mopos::Sensors::Schema::Result::Sensor_view;

use base qw/Mopos::Sensors::Schema::Result::Sensor/;

__PACKAGE__->table('sensors_view');

__PACKAGE__->add_columns(
    qw(
        center_name
        region_id
        region_name
        fullname_ascii
    ),
);

__PACKAGE__->has_many(
    'values',
    'Mopos::Sensors::Schema::Result::Value',
    'sensor_id',
);

__PACKAGE__->belongs_to(
    'sensor',
    'Mopos::Sensors::Schema::Result::Sensor',
    'id',
);
1;
