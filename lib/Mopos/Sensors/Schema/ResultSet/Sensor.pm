package Mopos::Sensors::Schema::ResultSet::Sensor;

use strict;
use warnings;
use utf8;

use base 'DBIx::Class::ResultSet';

our $VERSION = 1;

use constant TYPE => {
    1 => 'Hladinové čidlo',
    2 => 'Srážkoměr',
    3 => 'Meteostanice - Teplota',
    4 => 'Meteostanice - Vlhkost vzduchu',
    5 => 'Meteostanice - Atmosférický tlak',
    6 => 'Meteostanice - Vítr - směr',
    7 => 'Meteostanice - Vítr - rychlost',
};

sub types {
    return [
        map {
            { value => $_, label => TYPE->{$_} }
        } ( sort keys %{ TYPE() } )
    ];
}

sub type_name {
    my $self  = shift;
    my $type = shift;
    return TYPE->{$type};
}

1;
