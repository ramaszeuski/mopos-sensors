package Mopos::Sensors::RainGauge;

#TRASH!!!

use strict;
use warnings;
use Date::Manip;

sub new {
    my $class = shift;
    my $args  = shift;

    my $self = {
        stack    => [],
        sum      => 0,
        interval => $args->{interval},
        limit    => $args->{limit},
        debug    => $args->{debug},
        alert    => undef,
    };

    bless ($self, $class);
    return $self;
}

sub fill {
    my $self = shift;
    my $rain = shift;
    my @stack = ();

    # novy zaznam
    if ( $self->{debug} ) {
        print join " ", '>', $rain->{timestamp}, $rain->{value};
        print "\n";
    }

    # nemanipulujeme s prazdnymi
    if ( $rain->{value} ) {
        push @{ $self->{stack}}, $rain;
        $self->{sum} += $rain->{value};
    }

    # cistka starych
    foreach my $record ( @{ $self->{stack}} ) {
        my $delta = DateCalc( $record->{timestamp}, $rain->{timestamp} );
        $delta = Delta_Format($delta, 2, '%mt');

        if ( $delta >= $self->{interval} ) {
            $self->{sum} -= $record->{value};
        }
        else {
            if ( $self->{debug} ) {
                print join " ", '     ', (
                    $self->{limit} . '/' . $self->{interval},
                    $record->{timestamp},
                    $record->{value},
                );
                print "\n";
            }
            push @stack, $record;
        }
    }

    if ( $self->{sum} > $self->{limit} ) {
        $self->{alert} = {
            sum   => $self->{sum},
            stack => \@stack,
        };
        $self->{sum}       = 0;
        @{ $self->{stack}} = ();
    }
    else {
        $self->{alert}     = undef;
        @{ $self->{stack}} = @stack;
    }

}

1;
