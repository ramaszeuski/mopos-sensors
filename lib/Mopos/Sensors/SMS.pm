package Mopos::Sensors::SMS;
use strict;
use LWP::UserAgent;
use URI::Escape;
use Encode qw(encode);
use XML::Simple;
use YAML;
use utf8;

sub new {
    my ( $self, %ARGS ) = @_;
    my $obj = {};
    bless $obj;
    $obj->{ua}  = LWP::UserAgent->new();
    $obj->{url} = $ARGS{api} . "?password=$ARGS{password}&login=$ARGS{username}";
    return $obj;
}

sub request {
    my ($self, $url) = @_;
    my $response = $self->{ua}->get($self->{url} . $url);
    die $response->status_line unless $response->is_success;
    return XMLin($response->decoded_content);
}

sub credit_info {
    my ($self, %ARGS) = @_;
    my $rc = $self->request('&action=credit_info');
    return $rc->{credit};
}

sub inbox {
    my ($self, %ARGS) = @_;
    my $rc = $self->request('&action=inbox');
    return $rc->{inbox};
}

sub inbox_delete {
    my ($self, %ARGS) = @_;
    my $rc = $self->request("&action=inbox&delete=1");
    return $rc->{inbox};
}

sub delivery_report {
    my ($self, %ARGS) = @_;
    my $rc = $self->request('&action=inbox');
    my @report = ();
    for (@{$rc->{inbox}{delivery_report}{item}}) {
    my %sms = %{$_};
	    next unless $sms{status};
    	next unless $sms{time} =~ /(\d{4})(\d\d)(\d\d)T(\d\d)(\d\d)(\d\d)/;
	    $sms{time} = "$1-$2-$3 $4:$5:$6";
	    push @report, \%sms;
    }
    return @report;
}

sub send_sms {
    my ($self, %ARGS) = @_;
    $ARGS{number} ||= $ARGS{phone};
    return "invalid number $ARGS{number}" unless $ARGS{number} =~ /^\+?\d+$/;
    my $message = URI::Escape::uri_escape($self->cz_off($ARGS{message}));
    my $rc = $self->request("&action=send_sms&number=$ARGS{number}&message=$message");
    return $rc;
}

sub cz_off {
 my ($self, $string) = @_;
#$string = encode("UTF-8", $string);
 $string =~ s/á/a/g;
 $string =~ s/č/c/g;
 $string =~ s/ď/d/g;
 $string =~ s/é/e/g;
 $string =~ s/ě/e/g;
 $string =~ s/í/i/g;
 $string =~ s/ĺ/l/g;
 $string =~ s/ň/n/g;
 $string =~ s/ó/o/g;
 $string =~ s/ô/o/g;
 $string =~ s/ö/o/g;
 $string =~ s/ř/r/g;
 $string =~ s/š/s/g;
 $string =~ s/ť/t/g;
 $string =~ s/ú/u/g;
 $string =~ s/ů/u/g;
 $string =~ s/ý/y/g;
 $string =~ s/ž/z/g;
 $string =~ s/Á/A/g;
 $string =~ s/Č/C/g;
 $string =~ s/Ď/D/g;
 $string =~ s/É/E/g;
 $string =~ s/Ě/E/g;
 $string =~ s/Í/I/g;
 $string =~ s/Ĺ/L/g;
 $string =~ s/Ľ/L/g;
 $string =~ s/ľ/l/g;
 $string =~ s/Ň/N/g;
 $string =~ s/Ó/O/g;
 $string =~ s/Ô/O/g;
 $string =~ s/Ö/O/g;
 $string =~ s/Ř/R/g;
 $string =~ s/Š/S/g;
 $string =~ s/Ť/T/g;
 $string =~ s/Ú/U/g;
 $string =~ s/Ů/U/g;
 $string =~ s/Ý/Y/g;
 $string =~ s/Ž/Z/g;
 return $string;
}
1;
