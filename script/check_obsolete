#!/usr/bin/perl

use strict;
use utf8;
use warnings;

use FindBin qw($Bin);
use lib "$Bin/../lib";
use YAML;
use Proc::ProcessTable;
use Sys::Syslog;
use Getopt::Long::Descriptive;
use Date::Manip;

use Mopos::Sensors::Schema;
use Mopos::Sensors::SMS;

our $VERSION = '0.1';

use constant OPTIONS  => [
  [ 'data|d:s',     "datový adresář" ],
  [ 'help|h',       "tato napověda" ],
];

my ( $opt, $usage ) = describe_options( '%c %o', @{ (OPTIONS) });

$usage->die() if $opt->help;

Date_Init("DateFormat=non-US");

#logovani
openlog('sensors', 'ndelay,pid', 'local0');

# kontrola zda uz to nebezi
my $pt = new Proc::ProcessTable;
foreach my $process ( @{$pt->table} ) {
    if (
        ( $process->fname eq ('check_obsolete') )
        &&
        ( $process->pid != $$ )
    ) {
        die ("Already running\n");
    }
}

# Inicializace spojeni z databazi
my $db = Mopos::Sensors::Schema->connect({
    dsn      => $ENV{DB_DSN},
    user     => $ENV{DB_USERNAME},
    password => $ENV{DB_PASSWORD},
});
msg('info', "Connected to database " . $ENV{DB_DSN});

# Inicializace sms konektora
my $sms = Mopos::Sensors::SMS->new(
    api      => $ENV{SMS_API},
    username => $ENV{SMS_USERNAME},
    password => $ENV{SMS_PASSWORD},
);

my $now = UnixDate('now', '%Y');

my $deadline = UnixDate(DateCalc('now', '-8 hour' ), '%Y-%m-%d %H:%M');

my $sensors = $db->resultset('Sensor_view')->search(
    {
        active      => 't',
        fault       => 'f',
    },
);

my @obsolete = ();

SENSOR:
while ( my $sensor = $sensors->next ) {
    if ( $sensor->last_change && Date_Cmp( $sensor->last_change, $deadline ) < 0 ) {
        if ( ! $sensor->obsolete ) {
            $sensor->sensor->update( { obsolete => 't'} );
            push @obsolete, $sensor->fullname_ascii;
        }
    }
    elsif( $sensor->obsolete ) {
        $sensor->sensor->update( { obsolete => 'f'} );
    }
}

exit if ! scalar @obsolete;

my %alert = (
    message => 'Zastarale udaje z cidel ' .  join (', ', @obsolete),
);

RECIPIENT:
foreach my $recipient ( split /\D+/, $ENV{MONITORING_NUMBERS} ) {
    my $rc = $sms->send_sms(
        %alert,
        phone => "+420$recipient",
    );

    $alert{rc} = $rc->{err};

    if ($alert{rc}) {
        syslog('err', "SMS ERROR $alert{rc}");
    }
    else {
        $alert{price}  = $rc->{price};
        $alert{sms_id} = $rc->{sms_id};
    }
}

sub msg {
    my $level = shift;
    my $msg   = shift;
    syslog($level, $msg);
    print "$level> $msg\n";
}

