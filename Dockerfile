FROM registry.gitlab.com/ramaszeuski/catalyst-base:0.3.1

RUN useradd --no-log-init -u 1000 -r -g users sensors

RUN apt-get update && apt-get install -y \
 libnet-ldap-perl \
 libproc-processtable-perl \
 libmath-round-perl \
 libhtml-tableextract-perl \
 libhtml-format-perl \
 libcatalyst-action-rest-perl \
 libspreadsheet-writeexcel-perl

ADD . /opt/app

ENV PERL5LIB=/opt/app/lib CATALYST_HOME=/opt/app

USER sensors
CMD /opt/app/script/mopos_sensors_server.pl
