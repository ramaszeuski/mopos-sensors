use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'Mopos::Sensors' }
BEGIN { use_ok 'Mopos::Sensors::Controller::Inbox' }

ok( request('/inbox')->is_success, 'Request should succeed' );
done_testing();
