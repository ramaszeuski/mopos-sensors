use strict;
use warnings;
use Test::More;

BEGIN { use_ok 'Catalyst::Test', 'Mopos::Sensors' }
BEGIN { use_ok 'Mopos::Sensors::Controller::Sensors' }

ok( request('/sensors')->is_success, 'Request should succeed' );
done_testing();
